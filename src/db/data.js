import uuid from "react-uuid";

const data = {
  booksData: [
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/4gHJ3zvpe4EdHYsrYQqCqBTqzP7iV4gNYd2tdpLR63i4xyjhvGH9Eji2RcxH.",
      currentPrice: 115000,
      discount: false,
      delivery: false,
      description: "ПОЧЕМУ? Природа. Вопросы и ответы для маленьких почемучек",
      category: "Bolalar va ota-onalar uchun"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/URLUjdOtcaP5ALwcHoHTkTaW9zVd7wbW4AVul2lzH3t1U8gd7SUG96rIPUmU.",
      currentPrice: 115000,
      discount: false,
      delivery: false,
      description: "ПОЧЕМУ? Мой мир. Вопросы и ответы для маленьких почемучек",
      category: "Bolalar va ota-onalar uchun"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/eQ8w8iR9GSo0bqSNRnv3p2g3xWAHU2eucCzpB811BiWK0w8fKvb8GdZulTzL.",
      currentPrice: 129000,
      discount: false,
      delivery: false,
      description: "ПОЧЕМУ? Моё тело",
      category: "Bolalar va ota-onalar uchun"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/u66r9JxZ9kqsrBoJoNleEkNF3OvRcuZHTKlso7FQUGPJBLfMF65uPwYUu9WV.",
      currentPrice: 129000,
      discount: false,
      delivery: false,
      description: "Ковальская Ю. А.Почему? Животные",
      category: "Bolalar va ota-onalar uchun"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/KTgxPoiL2NVZaFCHd5T0no1inUMNkaGwnzKEUOBccYZkvyssL4LLV8fOskEq.",
      currentPrice: 85000,
      discount: false,
      delivery: false,
      description: "Почему у слоненка длинный хобот?",
      category: "Bolalar va ota-onalar uchun"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/eLvZdXWLiFEc4ZzXjBdvfKetdob8k2JGK0DDMNf3KO7KNar8VtRWccEWhkS6.",
      currentPrice: 159000,
      discount: false,
      delivery: false,
      description: "Почему отличники работают на троечников, а хорошисты на государство?",
      category: "Biznes kitoblar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/LVysWATCySA7UAR3MtFWYFSB4CQkXoH4YZ1jGdzC2qBYrXfIQAmaykWp2EQN.",
      currentPrice: 2085000,
      discount: false,
      delivery: false,
      description: "Почему мы хотим, чтобы вы были богаты",
      category: "Biznes kitoblar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/cz7AWBs9AuuzPgIx564gVeQd3NDG6iise4opFWzFcXRd5vnNBQlAqzTQqTnm.",
      currentPrice: 119000,
      discount: false,
      delivery: false,
      description: "Новикова Татьяна Олеговна: Почему мужчины врут, а женщины ревут",
      category: "Sog'liq, go'zallik, psixologiya"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/ln2S9UQ88DXQmYgaI0WexiC4g7qN2WJ9coNFyHlaoUkswLDQCRhqgg323nz1.",
      currentPrice: 99000,
      discount: false,
      delivery: false,
      description: "Почему вы глупы, больны и бедны… И как стать умным, здоровым и богатым",
      category: "TOP-100 Bestseller"
    },
  ],
  brendsData: [
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Kh/1646038495.jpg",
      brendName: "apple"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Ko/1646038649.jpg",
      brendName: "samsung"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Kd/1646038705.jpg",
      brendName: "sony"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Ka/1646038814.jpg",
      brendName: "versace"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Kp/1646038892.jpg",
      brendName: "xiaomi"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/KB/1646038994.jpg",
      brendName: "LG"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Kf/1646039060.jpg",
      brendName: "midea"
    },
    
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/KS/1628593446.jpg",
      brendName: "daewoo"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/Kc/1646039131.jpg",
      brendName: "acer"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/KO/1646039181.jpg",
      brendName: "asus"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/K6/1646039238.jpg",
      brendName: "HP"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/K-/1646039310.jpg",
      brendName: "lenovo"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/K3/1646039369.jpg",
      brendName: "dell"
    },
    {
      id: uuid(),
      brendImgUrl: "https://olcha.uz/uploads/images/manufacturer/KK/KK/KR/1646039445.jpg",
      brendName: "philips"
    }
  ],
  mobileData: [
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/rEES4fDb0cYy8rm4TsLWVIoh1FQeCHDj7WIT4d977eQpHrVrxNQwJxKJVDzL.",
      currentPrice: 3167000,
      discount: false,
      delivery: true,
      description: "Smartfon Redmi Xiaomi Note 11 Pro 128GB 8GB Grey",
      category: "Smartfonlar",
      favorite: false
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/Q25xwR9W7nPpnHwKTdWcPs0yHQPLLG9u1z6P5Z88f8RePUN8hUCJbkQo7ipz.jpeg",
      currentPrice: 2442000,
      discount: false,
      delivery: false,
      description: "Smartfon Redmi Xiaomi Note 11 Pro 128GB 6GB Grafit",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/YqLPUnnHd4PKlO3vEbCXpcvDSo3cMoxpNoQeVpaASr6HLOyh7QxVs1ewDzhQ.jpeg",
      currentPrice: 3399000,
      discount: true,
      discountPercent: 18,
      delivery: false,
      description: "Smartfon Samsung Galaxy A32 128GB Black",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/rEES4fDb0cYy8rm4TsLWVIoh1FQeCHDj7WIT4d977eQpHrVrxNQwJxKJVDzL.",
      currentPrice: 3167000,
      discount: false,
      delivery: true,
      description: "Smartfon Redmi Xiaomi Note 11 Pro 128GB 8GB Blue",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/mCX3ZIDmrPkaPNM3N4CApC2C8gNDjwO8HYUED33269e0Npt7UrLBvNH2h9lw.",
      currentPrice: 5408000,
      discount: false,
      delivery: false,
      description: "Smartfon Samsung Galaxy A73 5G 6/128GB Grey",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/QZFwvYXeNLbtnvf6bY1aR1bUZsyDzxyvOoLHMSj5AlKU3FwcQwTHacJb6BT3.",
      currentPrice: 2085000,
      discount: false,
      delivery: false,
      description: "Smartfon Samsung Galaxy A13  4/64GB Black",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/QZFwvYXeNLbtnvf6bY1aR1bUZsyDzxyvOoLHMSj5AlKU3FwcQwTHacJb6BT3.",
      currentPrice: 2085000,
      discount: false,
      delivery: false,
      description: "Smartfon Samsung Galaxy A13  4/64GB Black",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/rEES4fDb0cYy8rm4TsLWVIoh1FQeCHDj7WIT4d977eQpHrVrxNQwJxKJVDzL.",
      currentPrice: 3167000,
      discount: false,
      delivery: true,
      description: "Smartfon Redmi Xiaomi Note 11 Pro 128GB 8GB Blue",
      category: "Smartfonlar"
    },
    {
      id: uuid(),
      imgUrl: "https://olcha.uz/image/200x200/products/YqLPUnnHd4PKlO3vEbCXpcvDSo3cMoxpNoQeVpaASr6HLOyh7QxVs1ewDzhQ.jpeg",
      currentPrice: 3399000,
      discount: true,
      discountPercent: 18,
      delivery: false,
      description: "Smartfon Samsung Galaxy A32 128GB Black",
      category: "Smartfonlar"
    },
  ],
  newsData: [
    {
      id: uuid(),
      views: 221,
      newsTitle: "Qaynoq havoda yanada qaynoq vaucherlar va sovg‘alar!",
      description: "Qaynoq havoda yanada qaynoq vaucherlar va sovg‘alar!Ham xarid, ham xaridingizga qo‘shimcha" +
        " sovg‘alar, syurprizli vaucherlar olish zo‘r-a? \"Shunaqasiyam bo‘larkanmi?\",-dey",
      createdAt: "19.07.2022"
    },
    {
      id: uuid(),
      views: 237,
      newsTitle: "Issiqdan ham, sovuqdan ham himoya qiladi!",
      description: "Issiqdan ham, sovuqdan ham himoya qiladi! Hozirgi kunda global isish jarayoni sodir bo‘layotgani" +
        " tufayli, ko‘pchilik bu issiqdan aziyat chekmoqda.",
      createdAt: "23.07.2022"
    },
    {
      id: uuid(),
      views: 325,
      newsTitle: "Quloqchinlarning \"doda\"si!",
      description: `Quloqchinlarning \"doda\"si! Gaming qirollari va qirolichalari uchun o‘yin zavqini yanada oshiradigan, shiddatli musiqalarga hamohang harakatlanishni sevadiganlarni Infinix'ning super oq va qora rangli`,
      createdAt: "20.07.2022"
    },
    {
      id: uuid(),
      views: 137,
      newsTitle: "Karkasli basseyn",
      description: `Yozni soz o‘tkazish kerak!Yoz fasli qaynoq quyoshi bilan turli noqulayliklarni keltirib chiqaradi. Haddan ortiq issiqlab ketishdan, ayniqsa, bolakaylar aziyat chekishadi. Bunday paytlarda iliqqi`,
      createdAt: "18.07.2022"
    },
    {
      id: uuid(),
      views: 437,
      newsTitle: "Issiqdan ham, sovuqdan ham himoya qiladi!",
      description: "Issiqdan ham, sovuqdan ham himoya qiladi! Hozirgi kunda global isish jarayoni sodir bo‘layotgani" +
        " tufayli, ko‘pchilik bu issiqdan aziyat chekmoqda.",
      createdAt: "22.07.2022"
    },
  ]
}

export default data