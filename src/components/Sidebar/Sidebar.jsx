import React from 'react';
import uuid from "react-uuid";


const sidebarCategoryList = [
  {
    id: uuid(),
    categoryName: "Kantselariya tovarlari",
    active: true
  },
  {
    id: uuid(),
    categoryName: "Smartfon, telefon, gadjet, aksessuarlar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Noutbuk, printer, kompyuterlar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Televizor, foto-video va audio",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Mebel",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Geymerlar uchun",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Maishiy texnika",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Barchasi oshxona uchun",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Kitoblar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Barchasi uy, ofis va bog' uchun",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Barchasi ta'mirlash va qurilish uchun",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Sport anjomlari",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Avto jihozlar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Go'zallik va salomatmlik",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Kiyim va poyabzallar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Barchasi bolalar uchun",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Elektrotransport",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Sovg'alar va suvenerlar",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Aqilli uy",
    active: false,
    
  },
  {
    id: uuid(),
    categoryName: "Qulay takliflar",
    active: false,
    
  },


]

const Sidebar = (props) => {
  const {closeModal, modal} = props
  
  const activeClass = `translate-x-0`
  
  return (
    <>
      <div
        className={(modal ? `block` : "hidden") + " " + `bg-opacity-80 h-screen fixed top-0 left-0 w-full bg-black z-40`}/>
      <div
        className={(modal ? activeClass : "-translate-x-full") + " " + `fixed top-0 left-0 bg-white w-10/12 h-full z-50 flex transition-all ease-in-out duration-500`}
      >
        <nav className="w-4/12 border-r p-4">
          <ul className="flex flex-col h-full  overflow-auto">
            {
              sidebarCategoryList.map((item, index) => (
                <li key={item.id}><a
                  className={(item.active ? "bg-red-500 text-white" : "") + " " + `text-md w-11/12 focus:bg-red-500 focus:text-white rounded-xl hover:text-red-600 text-black mb-2 py-2 px-3 inline-block`}
                  href="#">{index + 1}. {item.categoryName}</a></li>
              ))
            }
          </ul>
        </nav>
        <div className="flex items-center justify-center flex-1">
          <h1 className="text-xl text-center font-bold">Content</h1>
        </div>
        <button onClick={closeModal}
                className="inline-block bg-red-600 text-center rounded-full m-2 text-white  w-8 h-8  z-50">&times;</button>
      </div>
    </>
  );
};

export default Sidebar;