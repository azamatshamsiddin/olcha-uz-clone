import React, { useEffect, useState } from 'react';
import { AiOutlineArrowUp } from "react-icons/ai";

const ScrollToTopBtn = () => {
  const [showTopBtn, setShowTopBtn] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 300) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);
  
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    })
  }
  return (
    <>
      {showTopBtn &&
        (
          <button onClick={goToTop}
                  className="fixed bottom-8 left-10 shadow-xl shadow-gray-700/30 rounded-full p-3.5 text-center cursor-pointer text-white bg-black hover:scale-110 transition-all ease-in-out duration-300">
            <AiOutlineArrowUp/>
          </button>
        )}
    </>
  );
};

export default ScrollToTopBtn;