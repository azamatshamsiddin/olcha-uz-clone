import React from 'react';
import { FiSearch } from "react-icons/fi";

const SearchBar = ({sticky}) => {
  return (
    <form onSubmit={(e) => e.preventDefault()}
          className={(sticky ? "border-black" : "border-red-50") + " " + `box-border border-2 items-center rounded-full flex flex-1`}>
      <input className="block py-2.5 px-4 flex-1 rounded-l-full outline-none text-black"
             placeholder="Mahsulotlarni qidirish..."
             type="text"/>
      <button
        className={(sticky ? "bg-black hover:bg-gray-800" : `bg-red-600 hover:bg-red-400`) + " " + `w-16 h-11 flex ml-auto items-center justify-center  text-white rounded-r-full transition ease-in-out duration-300`}>
        <FiSearch size="25"/>
      </button>
    </form>
  );
};

export default SearchBar;