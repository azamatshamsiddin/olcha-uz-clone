import React from 'react';
import LogoBrendWhite from "../../assets/logo-white.png";
import LogoBrendRed from "../../assets/logo-red.png";

const HeaderLogo = ({sticky}) => {
  
  return (
    <a href="" className="sm:hidden md:block mr-7">
      <img
        className="hover:opacity-80 transition-all duration-300 ease-in-out object-contain" width="130"
        src={sticky ? LogoBrendRed : LogoBrendWhite} alt="logo-brand"/>
    </a>
  );
};

export default HeaderLogo;