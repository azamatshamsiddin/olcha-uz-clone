import React, { useState } from 'react';

const FavoriteList = ()=>{
  return (
    <div className="w-52 h-24 bg-white shadow-xl rounded-xl p-3">
      <p>Lorem ipsum dolor.</p>
    </div>
  )
}

const HeaderMenuItem = (props) => {
  const {icon, title, sticky, favorite} = props
  const [showList, setShowList] = useState(false)
  return (
    <div>
      <li className="ml-10 md:ml-7">
        <a href="#"
           onClick={()=>setShowList(false)}
           className={(sticky ? "text-black hover:text-red-500" : `hover:text-red-500`) + " " + `relative transition ease-in-out duration-300 flex flex-col items-center font-bold`}>
          {icon}
          {title === "Sevimlilar" ?
            <span className="absolute -top-3 right-2 w-6 h-6 bg-black text-white flex items-center justify-center text-sm rounded-full">{favorite.length}</span> : ""
          }
          <p className="sm:hidden md:block ">{title}</p>
        </a>
      </li>
    </div>
  )
};

export default HeaderMenuItem;