import React from 'react';
import { IoIosArrowForward } from "react-icons/io";

const SectionTitle = ({title, navigateTitle="Hammasini ko'rish"}) => {
  return (
    <div className="section-top flex items-center justify-between">
      <h2 className="text-xl">{title}</h2>
      <a href="#"
         className="flex items-center text-base font-bold text-red-600 transition duration-300 translate-x-0 hover:translate-x-2">
        {navigateTitle} <IoIosArrowForward/>
      </a>
    </div>
  );
};

export default SectionTitle;