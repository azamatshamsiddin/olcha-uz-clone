import React, { useContext } from 'react';
import { FiHeart } from "react-icons/fi";
import {FaHeart} from "react-icons/fa"
import { CgMailForward } from "react-icons/cg";
import { BiBarChart } from "react-icons/bi";
import { SplideSlide } from "@splidejs/react-splide";
import { BookmarkContext } from "../../../App";


const SplideCard = ({item, onAdd}) => {
const {favorite} = useContext(BookmarkContext)
  return (
    <SplideSlide className="flex items-start justify-center">
      <div
        className="border-solid border relative py-7 px-4 flex h-full flex-col items-start w-full justify-between hover:shadow-2xl transition-all ease-in duration-200 cursor-pointer rounded-2xl">
        <span onClick={()=>onAdd(item)} className="hover:animate-heartbeat top-4 right-4 text-red-600 absolute">
         {
           favorite.includes(item) ? <FaHeart size="20"/> : <FiHeart size="20"/>
         }
        </span>
        <span className="rounded-full aspect-square p-1 hover:bg-red-400 top-12 right-2 text-red-600 absolute"><CgMailForward size="25"/></span>
        <img className="mb-2.5 self-center " src={item.imgUrl} alt={item.category}/>
        <a href="#" className={"text-base hover:text-red-600"}>{item.category}</a>
        <p className="font-bold text-lg h-8">{item.currentPrice.toLocaleString("uk")} so'm</p>
        <p className="text-base font-light">{item.description}</p>
        <div className="card-bottom w-full flex items-center justify-around">
          <button
            className="transition-all duration-300 ease-in-out text-base border-solid border-black border shadow-lg hover:border-red-200 hover:bg-red-600 hover:text-white w-40 rounded-full px-6 py-2 font-medium">Xarid
            qilish
          </button>
          <a className={"inline-block"} href="#">
            <BiBarChart className={"hover:text-red-600 transition-all duration-300 ease-in-out"} size={30}/>
          </a>
        </div>
      </div>
    </SplideSlide>
  );
};

export default SplideCard;