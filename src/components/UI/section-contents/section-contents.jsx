import React from 'react';
import { IoIosArrowForward } from "react-icons/io";
import { Splide, SplideTrack } from "@splidejs/react-splide";
import SplideCard from "../splide-card/splide-card";
import SectionTitle from "../section-title/section-title";

const SectionContents = (props) => {
  const {title, data, onAdd}= props
  return (
    <section className={"popular-goods container mx-auto px-2 mt-14"}>
     <SectionTitle title={title}/>
      <div className="mt-5">
        <Splide
          options={{
            rewind: true,
            arrows: true,
            pagination: false,
            perPage: 5,
            width: "inherit",
            height: 450,
            gap: "1rem",
            perMove: 1,
          }}
          hasTrack={false}
          aria-label="Prroducts"
        >
          <SplideTrack className="px-3 w-full">
            {data.map((item) => (
              <SplideCard  onAdd={onAdd} item={item} key={item.id} />
            ))}
          </SplideTrack>
        
        </Splide>
      </div>
    </section>
  );
};

export default SectionContents;