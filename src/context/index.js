import React, { createContext, useState } from "react";


const ModalContext = createContext(null)
export const ModalProvider = ({children}) => {
  const  [ modal, setModal] = useState(false)
  
  return (
    <ModalContext.Provider value={{setModal, modal}}>
      {children}
    </ModalContext.Provider>
  )
}