import React, { createContext, useState } from "react";
import "./App.scss";
import Header from "./layouts/header/header";
import DATA from "./db/data";
import Main from "./layouts/main/main";
import ScrollToTopBtn from "./components/scroll-top-btn/scroll-to-top-btn";
import Sidebar from "./components/Sidebar/Sidebar";

export const BookmarkContext = createContext(null)

function App() {
  const [data, setData] = useState(DATA)
  const [modal, setModal] = useState(false)
  const [favorite, setFavorite] = useState([])
  
  const addToFavorite = (item) => {
    if (!favorite.includes(item)) {
      setFavorite((prev) => [...prev, item])
    }

    if (favorite.includes(item)) {
      const newArr = favorite.filter(el => el.id !== item.id)
      setFavorite(newArr)
    }
  }
  
  const openModal = () => {
    setModal(true)
  }
  
  const closeModal = () => {
    setModal(false)
  }
  
  return (
    <BookmarkContext.Provider value={{favorite}}>
      <div className={""}>
        <Sidebar closeModal={closeModal} modal={modal}/>
        <Header favorite={favorite} openModal={openModal}/>
        <Main   data={data} onAdd={addToFavorite}/>
        <ScrollToTopBtn/>
      </div>
    </BookmarkContext.Provider>
    
   
  )
}

export default App;
