import React from 'react';
import SplideSlider from "../carousel/products-carousel";
import BannerSlider from "../carousel/banner-slider";
import SectionPopularGoods from "../section-popular-goods/section-popular-goods";
import SectionNewProducts from "../section-new-products/section-new-products";
import SectionBrends from "../../components/section-brends/section-brends";
import SectionNews from "../../components/section-news/section-news";
import Footer from "../footer/footer";
import SectionAboutUs from "../sction-about-us/section-about-us";


const Main = ({data, onAdd}) => {
  
  return (
    <main>
      <SplideSlider/>
      <BannerSlider/>
      <SectionPopularGoods onAdd={onAdd} data={data.mobileData} title={"Ommabop" +
        " tovarlar"}/>
      <SectionNewProducts onAdd={onAdd} data={data.booksData} title={"Yangi mahsulotlar"}/>
      <SectionBrends data={data.brendsData} title={"Brendlar"}/>
      <SectionNews  data={data.newsData} title={"Yangiliklar"}/>
      <SectionAboutUs/>
      <Footer/>
    </main>
  );
};

export default Main;