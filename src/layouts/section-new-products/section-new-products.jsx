import React from 'react';
import SectionContents from "../../components/UI/section-contents/section-contents";

const SectionNewProducts = (props) => {
  const {title, data, onAdd} = props
  return (
    <SectionContents data={data} onAdd={onAdd} title={title}/>
  );
};

export default SectionNewProducts;