import React from 'react';
import '@splidejs/splide/css';
import { Splide, SplideSlide, SplideTrack } from "@splidejs/react-splide";

const data = [
  {
    id: 1,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/xDc5cm2Bz0HvJ7Z8wOfsTJN0C0qiw22WbuQksyJEMPveKyqphLPxPOJYmnqa.",
  },
  {
    id: 2,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/Sv4mPz3gofe0quhLrU0Vef73kTX9ZAnvPyo26QTgnwCSTAO1byv7a59SsxfT.jpeg",
  },
  {
    id: 3,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/QB8Q9pI4BYiBrPFnLJGjPXmWx5yDY7WT6w1N4D1jsZ2k1NS0dFav3kixfIwh.jpeg",
  },
  {
    id: 4,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/PcqX0pWhHRreHXm4cqC8qf6ndYh12WFAcbee9wC4TaT99FWh3pg3Af330KDB.",
  },
  {
    id: 5,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/lWew3FtcULt4TZSMBpryOcVt5ZInui4gVa3kE1g20oFhWZZyCUvIj7ymPcV0.",
  },
  {
    id: 6,
    imgUrl: "https://olcha.uz/image/1400x360/sliders/oz/DFbyPImj9lknpTvEH7Q9A0FdSWn6U2jQCyNpbdj5IJMZrNpwIkerh3iwclVq.jpeg",
  }
]

const BannerSlider = () => {
  
  return (
    <section className={"banner container px-2 my-4 mx-auto mt-10"}>
      <Splide options={{
        rewind: true,
        tag: "section",
        arrow: true,
        pagination: true,
        paginationDirection: 'ltr',
        perPage: 1,
        width: "inherit",
        height: 360,
        type: 'loop',
        gap: '1rem',
        autoplay: true,
        perMove: 1,

        
      }} hasTrack={false} aria-label="Prroducts">
        <SplideTrack className="px-3 w-full rounded-2xl">
          {
            data.map(item => (
              <SplideSlide key={item.id}>
                <a className="w-full" href="#">
                  <img className={""} src={item.imgUrl} alt={item.id}/>
                </a>
              </SplideSlide>
            ))
          }
        </SplideTrack>
      </Splide>
    
    </section>
  
  
  );
};


export default BannerSlider;