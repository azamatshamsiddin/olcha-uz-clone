import React from 'react';

const SectionAboutUs = () => {
  return (
    <section className="container mx-auto px-2 mt-32">
      <h2 className="text-xl"><span className=" text-red-600 font-bold">Olcha</span> online do'koni - xarid qilish uchun
        qulay gipermarket</h2>
      <p className="mt-8 text-gray-500 text-md">
        Har qanday insoning hayoti va qulay sharoitlarini zamonaviy texnologiyalarsiz tasavvur qilib boʼlmaydi. Ish va
        shaxsiy savollar - smartfonda, sevimli filmlar, seriallar va telekoʼrsatuvlar - televizor, tozalash,
        yigʼishtirish – changyutgich, pishirish, tayyorlash - pechka, pech va koʼplab kichikroq, ammo undan kam
        boʼlmagan foydali qurilmalar va mahsulotlar. Bundan tashqari, texnologiyalar rivojlanmoqda va shuning uchun
        jihozlar muntazam yangilanishlarni talab qiladi, men yangi modellarni sinab, tekshirib koʼrishni xohlayman lekin
        mavjud qurilmalar va texnikalarning kuchi yetarli emas. Shu nuqtai nazardan, maishiy texnika sotib olish uchun
        xavfsiz va qulay joy, online-doʼkon boʼlishi foydalidir. Аgar siz hali oʼzingiz uchun mahsulot - tovar topa
        olmagan boʼlsangiz, biz sizni veb-sahifamizdada bir necha daqiqa qolishga taklif qilamiz. Sizni ishontirib
        aytamizki, vaqtingizni behuda sarf qilmaganligingizga ishonch hosil qilasiz.
      </p>
    </section>
  );
};

export default SectionAboutUs;