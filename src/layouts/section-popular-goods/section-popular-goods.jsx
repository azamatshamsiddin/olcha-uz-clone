import React from 'react';
import SectionContents from "../../components/UI/section-contents/section-contents";

const SectionPopularGoods = (props) => {
  const {data, title, onAdd} = props
  return (
    <SectionContents onAdd={onAdd}  data={data} title={title}/>
  );
};

export default SectionPopularGoods;