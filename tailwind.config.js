module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1140px',
      xl: '1400px',
    },
    fontSize: {
      sm: ['14px', '20px'],
      base: ['17px', '20px'],
      md: ['18px', '24px'],
      lg: ['22px', '24px'],
      xl: ['36px', '30px'],
    },

    extend: {
      animation: {
        'heartbeat': 'heartbeat .8s infinite',
      },
      keyframes: {
        heartbeat: {
          '0%': {transform: ' scale( .95 )'},
          '90%': {transform: ' scale( 1.1 )'},
          '100%': {transform: ' scale( 1 )'},
        }
      }
    },
  },
  plugins: [],
}